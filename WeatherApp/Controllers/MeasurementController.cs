﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WeatherApp.Models;

namespace WeatherApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MeasurementController : ControllerBase
    {
        public WebClient client = new WebClient();

        // GET api/station
        [HttpGet]
        public JsonResult Get()
        {
            string stationURL = "https://danepubliczne.imgw.pl/api/data/synop/station/hel";         
            WeatherState measurement = JsonConvert.DeserializeObject<WeatherState>(client.DownloadString(stationURL));

            return new JsonResult(measurement) { StatusCode = 200, ContentType = "application/json" };
        }
    }
}