﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using WeatherApp.Helpers;
using WeatherApp.Models;

namespace WeatherApp.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ForecastController : ControllerBase
    {
        public WebClient client = new WebClient();

        // GET api/forecast/10
        [HttpGet("{hour}")]
        public JsonResult Get(int hour)
        {
            string windSpeed;
            string forecastUrl = "http://www.pogodynka.pl/polska/hel_hel";

            string weatherToday = StringHelper.getSubstringBetween(client.DownloadString(forecastUrl), $"Dzisiaj godz. {hour}:00", "<div class='cosmo_desc'>model:<br><p>COSMO</p></div></div>");
            string tmp = StringHelper.getSubstringBetween(weatherToday, "Prędkość wiatru", " m/s");
            if (tmp.Length == 60) windSpeed = tmp.Substring(tmp.Length - 1);
            else windSpeed = tmp.Substring(tmp.Length - 2);

            WeatherState forecast = new WeatherState() { predkosc_wiatru = windSpeed };

            return new JsonResult(forecast) { StatusCode = 200, ContentType = "application/json"};
        }
    }
}