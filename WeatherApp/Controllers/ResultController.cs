﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using WeatherApp.Models;

namespace WeatherApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResultController : ControllerBase
    {
        static HttpClient client = new HttpClient();

        static async Task<WeatherState> GetForecast(int hour)
        {
            string forecastUrl = $"https://localhost:44396/api/forecast/{hour}";
            HttpResponseMessage response = await client.GetAsync(forecastUrl);
            WeatherState forecastState = null;
            if (response.IsSuccessStatusCode) forecastState = await response.Content.ReadAsAsync<WeatherState>();
            return forecastState;
        }

        static async Task<WeatherState> GetMeasurement(int hour)
        {
            string measurementUrl = "https://localhost:44396/api/measurement";
            WeatherState measurementState = null;
            do
            {
                Thread.Sleep(600000);
                HttpResponseMessage response = await client.GetAsync(measurementUrl);
                if (response.IsSuccessStatusCode) measurementState = await response.Content.ReadAsAsync<WeatherState>();
            }           
            while (!hour.Equals(measurementState.godzina_pomiaru));
           
            return measurementState;
        }

        // GET api/result/10
        [HttpGet("{hour}")]
        public async void Get(int hour)
        {
            WeatherState forecast = await GetForecast(hour);
            WeatherState measurement = await GetMeasurement(hour);

            int result = Int32.Parse(measurement.predkosc_wiatru) - Int32.Parse(forecast.predkosc_wiatru);

            if (result == 0) System.IO.File.WriteAllText($@"C:\WeatherApp\result{DateTime.Now.Day}_{hour}.json",
                $"Current wind speed as expected: {measurement.predkosc_wiatru} m/s");

            else if (result > 0) System.IO.File.WriteAllText($@"C:\WeatherApp\result{DateTime.Now.Day}_{hour}.json",
                $"Current wind speed ({measurement.predkosc_wiatru} m/s) is higher than expected ({forecast.predkosc_wiatru} m/s) by {result} m/s");

            else System.IO.File.WriteAllText($@"C:\WeatherApp\result{DateTime.Now.Day}_{hour}.json",
                $"Current wind speed ({measurement.predkosc_wiatru} m/s) is lower than expected ({forecast.predkosc_wiatru} m/s) by {result * (-1)} m/s");
        }
    }
}
